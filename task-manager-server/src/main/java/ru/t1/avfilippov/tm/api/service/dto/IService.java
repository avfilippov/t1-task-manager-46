package ru.t1.avfilippov.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.api.repository.dto.IDTORepository;
import ru.t1.avfilippov.tm.dto.model.AbstractModelDTO;
import ru.t1.avfilippov.tm.enumerated.Sort;

import java.util.List;

public interface IService<M extends AbstractModelDTO> extends IDTORepository<M> {

    @Nullable
    List<M> findAll(@Nullable Sort sort);

    void removeById(@Nullable String id);

}
