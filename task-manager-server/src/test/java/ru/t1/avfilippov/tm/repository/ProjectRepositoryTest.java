package ru.t1.avfilippov.tm.repository;

import org.junit.experimental.categories.Category;
import ru.t1.avfilippov.tm.marker.DataCategory;

@Category(DataCategory.class)
public final class ProjectRepositoryTest {
/*
    @NotNull
    private final IConnectionService connectionService = new ConnectionService(new PropertyService());

    @NotNull
    private final IProjectRepository projectRepository = connectionService.getSqlSession().getMapper(IProjectRepository.class);

    @Before
    public void before() {
        projectRepository.add(USER_PROJECT1);
        projectRepository.add(ADMIN_PROJECT1);
    }

    @After
    public void after() {
        projectRepository.clear();
    }

    @Test
    public void add() {
        @Nullable final Project project = projectRepository.findOneById(USER_PROJECT2.getUserId(),USER_PROJECT2.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT2, project);
    }

    @Test
    public void addByUserId() {
        @Nullable final Project project = projectRepository.findOneById(USER_PROJECT2.getUserId(), USER_PROJECT2.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT2, project);
    }

    @Test
    public void createdByUserId() {
        Assert.assertEquals(ADMIN_PROJECT1.getUserId(), USER2.getId());
    }

    @Test
    public void findAll() {
        @NotNull final ConnectionService connectionService = new ConnectionService(new PropertyService());
        @NotNull final IProjectRepository emptyRepository =connectionService.getSqlSession().getMapper(IProjectRepository.class);
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_PROJECT1);
        Assert.assertEquals(USER_PROJECT1, emptyRepository.findOneById(USER_PROJECT1.getUserId(),USER_PROJECT1.getId()));
    }

    @Test
    public void findById() {
        Assert.assertNotNull(projectRepository.findOneById(USER1.getId(), USER_PROJECT1.getId()));
    }

    @Test
    public void removeById() {
        Assert.assertNotNull(projectRepository.removeById(ADMIN_PROJECT1.getId()));
    }

    @Test
    public void removeByIndex() {
        Assert.assertNotNull(projectRepository.removeByIndex(1));
    }

    @Test
    public void removeAll() {
        projectRepository.clear();
        Assert.assertTrue(projectRepository.findAll().isEmpty());
    }

    @Test
    public void create() {
        @NotNull final Project project = projectRepository.create(USER2.getId(), ADMIN_PROJECT1.getName(), ADMIN_PROJECT1.getDescription());
        Assert.assertEquals(ADMIN_PROJECT1.getDescription(), project.getDescription());
        Assert.assertEquals(ADMIN_PROJECT1.getName(), project.getName());
        Assert.assertEquals(USER2.getId(), project.getUserId());
    }
 */
}
